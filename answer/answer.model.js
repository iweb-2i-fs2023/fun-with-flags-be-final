const answers = [];

function create(answer) {
  const newAnwser = {
    id: getNextId(),
    questionId: answer.questionId,
    optionNrChosen: answer.optionNrChosen,
  };
  answers.push(newAnwser);
  return Promise.resolve(newAnwser);
}

function getNextId() {
  if (!answers || answers.length === 0) {
    return '1';
  }
  return '' + (parseInt(answers[answers.length - 1].id) + 1);
}

export { create };
