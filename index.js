import express from 'express';
import { loadCountries } from './country/country.controller.js';
import { router as questionRouter } from './question/question.routes.js';
import { router as answerRouter } from './answer/answer.routes.js';

const app = express();

app.use(express.json());
app.use('/api/questions', questionRouter);
app.use('/api/answers', answerRouter);

await loadCountries();
app.listen(3001, async () => {
  console.log('Server listens to http://localhost:3001');
});
